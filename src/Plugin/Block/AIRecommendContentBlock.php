<?php

namespace Drupal\recommend_ai\Plugin\Block;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;
use Drupal\openai_embeddings\VectorClientPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'AI Recommend Content' block.
 *
 * @Block(
 *   id = "ai_recommend_content",
 *   admin_label = @Translation("AI Recommend Content"),
 * )
 */
class AIRecommendContentBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Stores the configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The vector client plugin manager.
   *
   * @var \Drupal\openai_embeddings\VectorClientPluginManager
   */
  protected $pluginManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match, ConfigFactoryInterface $config_factory, VectorClientPluginManager $plugin_manager, EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepositoryInterface $entity_display_repository, EntityFieldManagerInterface $entity_field_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->routeMatch = $route_match;
    $this->configFactory = $config_factory;
    $this->pluginManager = $plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->entityFieldManager = $entity_field_manager;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('config.factory'),
      $container->get('plugin.manager.vector_client'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'view_mode' => '',
      'recommendation_count' => 1,
      'field' => '',
      'type_of_content' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    // Get the list of all the available view modes.
    $view_modes = $this->entityDisplayRepository->getViewModes('node');

    $entity_types = $this->configFactory->get('openai_embeddings.settings')->get('entity_types');

    $fields_list = [];
    $bundles = [];
    // Prepare the field names for all the selected bundles.
    foreach ($entity_types['node'] ?? [] as $value) {
      $fields = $this->entityFieldManager->getFieldDefinitions('node', $value);

      foreach ($fields as $field) {
        if (in_array($field->getType(), [
          'string',
          'string_long',
          'text',
          'text_long',
          'text_with_summary',
        ])) {
          $fields_list[$field->getName()] = $field->getLabel();
        }
      }
      // Prepare the list of bundles.
      $bundles[$value] = $this->entityTypeManager->getStorage('node_type')->load($value)->label();
    }

    $options = [];
    foreach ($view_modes as $key => $value) {
      $options[$key] = $value['label'];
    }

    $form['view_mode'] = [
      '#type' => 'select',
      '#default_value' => $this->configuration['view_mode'] ?? '',
      '#options' => $options,
      '#required' => TRUE,
      '#description' => $this->t('Select the view mode which will be used to show the recommended content.'),
      '#title' => $this->t('View Mode'),
    ];

    $form['recommendation_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Content count'),
      '#min' => 1,
      '#default_value' => $this->configuration['recommendation_count'],
      '#description' => $this->t('Add the count of recommended content that should be visible.'),
      '#required' => TRUE,
    ];

    $form['type_of_content'] = [
      '#type' => 'select',
      '#title' => $this->t('Type of Content'),
      '#required' => TRUE,
      '#options' => $bundles,
      '#default_value' => $this->configuration['type_of_content'],
      '#description' => $this->t('Select the type of content which should be shown as recommendation.'),
    ];

    $form['field'] = [
      '#type' => 'radios',
      '#title' => $this->t('Field'),
      '#default_value' => $this->configuration['field'],
      '#options' => $fields_list,
      '#description' => $this->t('Select the field name based on which the content recommendation will be generated.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['view_mode'] = $form_state->getValue('view_mode');
    $this->configuration['recommendation_count'] = $form_state->getValue('recommendation_count');
    $this->configuration['field'] = $form_state->getValue('field');
    $this->configuration['type_of_content'] = $form_state->getValue('type_of_content');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = $this->routeMatch->getParameter('node');
    $recommended_items = [];
    $cache_tags = [];
    $build = [];

    if ($node instanceof NodeInterface) {
      // You can get nid and anything else you need from the node object.
      $nid = $node->id();
      $bundle = $node->bundle();
      $entity_type = $node->getEntityTypeId();
      $field = $this->configuration['field'] ?? '';

      // @todo To make it more generic based on the type of field.
      $source_id = "entity:$nid:$entity_type:$bundle:$field:0";
      // Get the vector data from the pinecone vector database.
      $plugin_id = $this->configFactory->get('openai_embeddings.settings')->get('vector_client_plugin');

      // @todo Add support of other plugins as well.
      if ($plugin_id === 'pinecone') {
        $vector_client = $this->pluginManager->createInstance($plugin_id);
        $pinecone_query = $vector_client->fetch([
          'source_ids' => $source_id,
          'collection' => $entity_type,
        ]);

        $result = Json::decode($pinecone_query->getBody()->getContents());

        if (!empty($result['vectors'])) {
          $current_page_vector = $result['vectors'][$source_id]['values'];

          $query = $vector_client->query([
            'vector' => $current_page_vector,
            'top_k' => $this->configuration['recommendation_count'],
            'collection' => $entity_type,
            'filter' => [
              'entity_type' => $entity_type,
              'entity_id' => [
                '$ne' => $nid,
              ],
              'field_name' => $this->configuration['field'],
              'bundle' => $this->configuration['type_of_content'],
            ],
          ]);

          $data = Json::decode($query->getBody()->getContents());
          $recommended_ids = [];
          // Traverse the data if not empty.
          if (!empty($data) && !empty($data['matches'])) {
            foreach ($data['matches'] as $value) {
              $recommended_ids[] = $value['metadata']['entity_id'];
            }
          }

          // Prepare the recommended items.
          foreach ($recommended_ids as $id) {
            $node = $this->entityTypeManager->getStorage('node')->load($id);
            // If indexed node does not exist in system anymore, this code should
            // not break. So check for empty node.
            if (!$node instanceof NodeInterface) {
              continue;
            }
            $cache_tags = array_merge($cache_tags, $node->getCacheTags());
            $node_view = $this->entityTypeManager->getViewBuilder('node')->view($node, $this->configuration['view_mode'] ?? 'teaser');
            $recommended_items[] = $node_view;
          }
        }
      }
    }

    // If recommended items is empty then don't render the block.
    if (!empty($recommended_ids)) {
      $build = [
        '#theme' => 'item_list',
        '#items' => $recommended_items,
        '#cache' => [
          'tags' => Cache::mergeTags($cache_tags, parent::getCacheTags()),
          'contexts' => $this->getCacheContexts(),
        ],
      ];

      CacheableMetadata::createFromRenderArray($build)->addCacheableDependency($this)->applyTo($build);
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $cache_contexts = parent::getCacheContexts();

    $cache_contexts[] = 'url.path';
    return $cache_contexts;
  }

}
